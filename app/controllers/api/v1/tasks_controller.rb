module Api
    module V1
        class TasksController < ApplicationController
            def index
                tasks = Task.order('created_at DESC');
                render json: {status: 'SUCCESS', data:tasks},status: :ok
            end

            def show
                task = Task.find(params[:id])
                render json: {status: 'SUCCESS', data:task},status: :ok
            end

            def create 
                task = Task.new(article_params)

                if task.save
                    render json: {status: 'SUCCESS', message:'Saved task', data:task},status: :ok
                else
                    render json: {status: 'ERROR', message: 'Article not saved', data:task.errors},status: :unprocessable_entry
                end
            end

            def destroy
                task = Task.find(params[:id])
                task.destroy
                render json: {status: 'SUCCESS', message:'Deleted task', data:task},status: :ok
            end

            def update
                task = Task.find(params[:id])
                if task.update_attributes(article_params)
                    render json: {status: 'SUCCESS', message:'Updated task', data:task},status: :ok
                else
                    render json: {status: 'ERROR', message: 'Article not Updated', data:task.errors},status: :unprocessable_entry
                end
            end

            private

            def article_params
                params.permit(:title)
            
            end
        end
    end
end